<?php
/**
 * @file
 * Handler for named user alias field.
 */

/**
 * Field handler to present the aliased name of a user.
 *
 * @ingroup views_field_handlers
 */
class named_users_handler_field_user_name_alias extends views_handler_field {

  function construct() {
    parent::construct();
    $this->additional_fields['uid'] = 'uid';
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields();
  }

  function render($values) {
    $uid = $this->get_value($values, 'uid');
    $alias = drupal_get_path_alias("user/$uid");

    return substr($alias, strrpos($alias, '/') + 1);
  }
}
