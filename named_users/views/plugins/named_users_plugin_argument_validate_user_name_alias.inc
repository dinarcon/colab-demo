<?php
/**
 * @file
 * Definition of validation plugin for using a named alias as contextual filter.
 */

/**
 * Validate whether an argument is a valid name alias.
 *
 * This supports either numeric arguments (UID) or strings (named alias) and
 * converts either one into the user's UID.  This validator also sets the
 * argument's title to the username.
 *
 * @see views_plugin_argument_validate_user
 */
class named_users_plugin_argument_validate_user_name_alias extends views_plugin_argument_validate {

  function option_definition() {
    $options = parent::option_definition();
    $options['type'] = array('default' => 'either');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    $form['type'] = array(
      '#type' => 'radios',
      '#title' => t('Type of user filter value to allow'),
      '#options' => array(
        'uid' => t('Only allow numeric UIDs'),
        'name' => t('Only allow string aliased names.'),
        'either' => t('Allow both numeric UIDs and string aliased names'),
      ),
      '#default_value' => $this->options['type'],
    );
  }

  function validate_argument($argument) {
    $type = $this->options['type'];
    // is_numeric() can return false positives, so we ensure it's an integer.
    // However, is_integer() will always fail, since $argument is a string.
    if (is_numeric($argument) && $argument == (int) $argument) {
      if ($type == 'uid' || $type == 'either') {
        if ($argument == $GLOBALS['user']->uid) {
          // If you assign an object to a variable in PHP, the variable
          // automatically acts as a reference, not a copy, so we use
          // clone to ensure that we don't actually mess with the
          // real global $user object.
          $account = clone $GLOBALS['user'];
        }
        else {
          $query = "SELECT uid, name FROM {users} WHERE uid = :argument";
          $account = db_query($query, array(':argument' => $argument))->fetchObject();
        }
      }
    }
    else {
      if ($type == 'name' || $type == 'either') {
        if ($uid = named_users_get_uid_from_name_alias($argument)) {
          $account = user_load($uid);
        }
      }
    }

    if (empty($account)) {
      // User not found.
      return FALSE;
    }

    $this->argument->argument = $account->uid;
    $this->argument->validated_title = check_plain(format_username($account));
    return TRUE;
  }
}
