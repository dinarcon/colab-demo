<?php
/**
 * @file
 * Views's API code.
 */

/**
 * Implements hook_views_data().
 */
function named_users_views_data() {
  $data['users']['name_alias'] = array(
    'field' => array(
      'title' => t('Name Alias'),
      'help' => t('The aliased name of this user.'),
      'handler' => 'named_users_handler_field_user_name_alias',
    ),
  );

  return $data;
}

/**
 * Implements hook_views_handlers().
 */
function named_users_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'named_users') . '/views/handlers',
    ),
    'handlers' => array(
      'named_users_handler_field_user_name_alias' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}

/**
 * Implements hook_views_plugins().
 */
function named_users_views_plugins() {
  return array(
    'argument validator' => array(
      'user_name_alias' => array(
        'title' => t('Name alias'),
        'handler' => 'named_users_plugin_argument_validate_user_name_alias',
        'path' => drupal_get_path('module', 'named_users') . '/views/plugins',
      ),
    ),
  );
}

/**
 * Gets the UID from a aliased name.
 *
 * @param string $alias
 *   The aliased name to check.
 *
 * @return int|bool
 *   The UID of the user if found, FALSE otherwise.
 */
function named_users_get_uid_from_name_alias($alias) {
  $query = db_select('url_alias', 'ua');
  $query->addExpression('SUBSTRING_INDEX( source,  :delimiter, -1 )', 'uid', array(':delimiter' => '/'));
  $query->where('ua.alias LIKE :alias', array(':alias' => 'users/' . $alias));
  $query->where('ua.source LIKE :pattern', array(':pattern' => 'user/%'));
  $query->range(0, 1);
  $result = $query->execute()->fetchObject();

  if ($result) {
    return $result->uid;
  }

  return FALSE;
}
